<?php
/**
 * Plugin Name: Drubo Portfolio
 * Description: Drubo Portfolio plugin
 * Author: Hastech
 * Author URI: http://hastech.company
 * version: 1.0.1
*/



require_once dirname(__FILE__) . '/posttype.php';
require_once dirname(__FILE__) . '/shortcode.php';


// Enable kc for Post 
if ( class_exists( 'KingComposer' ) ) {
	// Enable kc for Post 
	global $kc;
	$kc->add_content_type( array( 'post', 'drubo-portfolio' ) );
}
