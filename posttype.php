<?php

if ( ! function_exists('drubo_custom_post_type_portfolio') ) {

// Register Custom Post Type
function drubo_custom_post_type_portfolio() {

	$labels = array(
		'name'                  => _x( 'Portfolios', 'Post Type General Name', 'drubo' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'drubo' ),
		'menu_name'             => __( 'Portfolio', 'drubo' ),
		'name_admin_bar'        => __( 'Portfolio', 'drubo' ),
		'archives'              => __( 'Portfolio Archives', 'drubo' ),
		'attributes'            => __( 'Item Attributes', 'drubo' ),
		'parent_item_colon'     => __( 'Parent Item:', 'drubo' ),
		'all_items'             => __( 'All Portfolios', 'drubo' ),
		'add_new_item'          => __( 'Add New Portfolio', 'drubo' ),
		'add_new'               => __( 'Add New Portfolio', 'drubo' ),
		'new_item'              => __( 'New Portfolio', 'drubo' ),
		'edit_item'             => __( 'Edit Portfolio', 'drubo' ),
		'update_item'           => __( 'Update Portfolio', 'drubo' ),
		'view_item'             => __( 'View Portfolio', 'drubo' ),
		'view_items'            => __( 'View Portfolios', 'drubo' ),
		'search_items'          => __( 'Search Item', 'drubo' ),
		'not_found'             => __( 'Not found', 'drubo' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'drubo' ),
		'featured_image'        => __( 'Featured Image', 'drubo' ),
		'set_featured_image'    => __( 'Set featured image', 'drubo' ),
		'remove_featured_image' => __( 'Remove featured image', 'drubo' ),
		'use_featured_image'    => __( 'Use as featured image', 'drubo' ),
		'insert_into_item'      => __( 'Insert into item', 'drubo' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Portfolio', 'drubo' ),
		'items_list'            => __( 'Items list', 'drubo' ),
		'items_list_navigation' => __( 'Items list navigation', 'drubo' ),
		'filter_items_list'     => __( 'Filter items list', 'drubo' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'drubo' ),
		'description'           => __( 'Portfolio Description', 'drubo' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'drubo_portfolio_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-screenoptions',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'drubo_portfolio', $args );

}
add_action( 'init', 'drubo_custom_post_type_portfolio', 0 );

}


if ( ! function_exists( 'drubo_custom_taxonomy_portfolio' ) ) {

// Register Custom Taxonomy
function drubo_custom_taxonomy_portfolio() {

	$labels = array(
		'name'                       => _x( 'Portfolio Categories', 'Taxonomy General Name', 'drubo' ),
		'singular_name'              => _x( 'Portfolio Category', 'Taxonomy Singular Name', 'drubo' ),
		'menu_name'                  => __( 'Portfolio Category', 'drubo' ),
		'all_items'                  => __( 'All Portfolio Categories', 'drubo' ),
		'parent_item'                => __( 'Parent Item', 'drubo' ),
		'parent_item_colon'          => __( 'Parent Item:', 'drubo' ),
		'new_item_name'              => __( 'New Portfolio Category', 'drubo' ),
		'add_new_item'               => __( 'Add New Portfolio Category', 'drubo' ),
		'edit_item'                  => __( 'Edit Portfolio Category', 'drubo' ),
		'update_item'                => __( 'Update Portfolio Category', 'drubo' ),
		'view_item'                  => __( 'View Item', 'drubo' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'drubo' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'drubo' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'drubo' ),
		'popular_items'              => __( 'Popular Items', 'drubo' ),
		'search_items'               => __( 'Search Items', 'drubo' ),
		'not_found'                  => __( 'Not Found', 'drubo' ),
		'no_terms'                   => __( 'No items', 'drubo' ),
		'items_list'                 => __( 'Items list', 'drubo' ),
		'items_list_navigation'      => __( 'Items list navigation', 'drubo' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'druboportfoliocat', array( 'drubo_portfolio' ), $args );

}
add_action( 'init', 'drubo_custom_taxonomy_portfolio', 0 );

}
